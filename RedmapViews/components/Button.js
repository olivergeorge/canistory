import * as React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Text, TouchableOpacity } from 'react-native';

export default function Button({ title, disabled, onPress }) {
  return (
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <Text>{title} {disabled ? "disabled": "enabled"}</Text>
      </TouchableOpacity>
  );
}

Button.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  disabled: PropTypes.bool.isRequired,
};