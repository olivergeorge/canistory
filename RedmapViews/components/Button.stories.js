import * as React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { boolean, text, object, withKnobs } from '@storybook/addon-knobs';

import Button from './Button';

storiesOf('Button', module)
  .addDecorator(withKnobs)
  .addDecorator(story => <View>{story()}</View>)
  .add('simple', () => <Button title="default" onPress={x=>console.log(x)} disabled={false} />)
  .add('action', () => <Button title="default" onPress={action('onPress')} disabled={false} />)
  .add('Prop knobs', () => <Button title={text('title', "Press me")} onPress={action('onPress')} disabled={boolean('disabled', false)} />)
  .add('Object knobs', () => <Button {...object('props', {'title': 'hello', 'disabled': false})} onPress={action('onPress')} />)
  ;
